var cassandra = require('cassandra-driver');
var uuid = require('node-uuid');

var CassandraPersister = function(config) {
    var cassandraConnect = config.cassandraConnect || {
        contactPoints: ['127.0.0.1']
    };

    this._cassandraClient = new cassandra.Client(cassandraConnect);

    var self = this;

    this._executeCassandraQuery = function(query, params, resolve, reject) {
        // TODO: isolate the persistance object from Cassandra-specific objects
        self._cassandraClient.execute(query, params, {prepare: true}, function(err, result) {
            if (err != null) {
                console.log(err);
                reject({message: 'No data returned.'});
            } else {
                resolve({data: result.rows});
            }
        });
    }

    this.save = function(contact) {
        var query = 'INSERT INTO contacts.contact (id, name, details) values (?, ?, ?);';

        var id = contact.id || uuid.v4();
        var name = contact.name || '';
        var details = contact.details;

        var promise = new Promise(function(resolve, reject){
            self._cassandraClient.execute(query, [id, name, details], {prepare: true}, function(err, result) {
                if (err != null) {
                    console.log(err);
                    reject({message: 'Insert failed.'});
                } else{
                    resolve({data: {id: id}});
                }
            })
        });

        return promise;
    };

    this.retrieve = function() {
        var query = 'SELECT id, name, details FROM contacts.contact;';

        var promise = new Promise(function(resolve, reject){
            self._executeCassandraQuery(query, [], resolve, reject);
        });

        return promise;
    }

    this.delete = function(contact) {
        query = 'DELETE FROM contacts.contact WHERE id = ?;'

        var promise = new Promise(function(resolve, reject){
            if (contact.id === undefined || contact.id == null) {
                reject({message: 'Contact ID undefined.'});
                return;
            } else {
                var id = contact.id; // This gets hoisted up
            }

            self._executeCassandraQuery(query, [id], resolve, reject);
        });

        return promise;
    }
};

module.exports = {
    CassandraPersister: CassandraPersister
};

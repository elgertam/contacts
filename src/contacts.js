var ContactList = function (persister) {
    this._persister = persister;
    var self = this;

    this.addOrUpdate = function(contact, successHandler, failureHandler) {
        self._persister
            .save(contact)
            .then(successHandler, failureHandler);

        return self;
    }

    this.retrieveAll = function (successHandler, failureHandler) {
        self._persister
            .retrieve()
            .then(successHandler, failureHandler);

        return self;
    }

    this.retrieveThisMany = function (count, successHandler, failureHandler) {
        // TODO: Implement

        return self;
    }

    this.retreiveById = function (id, successHandler, failureHandler) {
        // TODO: Implement
        return self;
    }

    this.remove = function(contact, successHandler, failureHandler) {
        self._persister
            .delete(contact)
            .then(successHandler, failureHandler);

        return self;
    }

    this.removeById = function(id, successHandler, failureHandler) {
        self._persister
            .delete({id: id})
            .then(successHandler, failureHandler);

        return self;
    }
}

module.exports = {
    ContactList: ContactList
};

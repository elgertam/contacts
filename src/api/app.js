var express = require('express');
var bodyParser = require('body-parser')

var contacts = require('../contacts');
var cassandra = require('../persistance/cassandra')

var config = {
    cassandraConnect: {
        contactPoints: ['127.0.0.1'],
        keyspace: 'contacts'
    },
    port: 3001
};

var persister = new cassandra.CassandraPersister(config);
var contactList = new contacts.ContactList(persister);

(function(config, contactList) {

    var app = express();

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
        next();
    });

    app.use(bodyParser.json());

    app.get('/', function(req, res){
        res.redirect('/contacts');
    })

    app.route('/contacts/:id')
        .delete(function(req, res){
            contactList.removeById(req.params.id, function(){
                res.send({status: 200});
            }, function(error){
                res.send({status: 500, message: error.message});
            });
        });

    app.route('/contacts')
        .get(function(req, res) {
            contactList.retrieveAll(function(result) {
                res.send({status: 200, message: 'success', data: result.data});
            }, function(error) {
                res.send({status: 500, message: error.message});
            });
        })
        .post(function(req, res){
            var contact = req.body;
            console.log(contact);
            contactList.addOrUpdate(contact, function(result){
                res.send({status: 200, message: 'success', data: result.data});
            }, function(error){
                res.send({status: 500, message: error.message});
            });
        })
        .delete(function(req, res){
            var contact = req.body;
            contactList.remove(contact, function(result){
                res.send({status: 200, message: 'success'});
            }, function(error){
                res.send({status: 500, message: error.message});
            });
        });


    app.listen(config.port);
})(config, contactList);

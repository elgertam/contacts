README
======

This is a simple contacts app written in Node.js using Express and Cassandra.

Installation
------------

1.  Clone repo & change to local directory
2.  `npm install`
3.  Set config parameters in `src/api/app.js`
4.  Run CQL script in `data/contacts.cql`
5.  `npm start`
// TODO: Refactor this into a real integration test w/ MochaJS

var cassandra = require('./src/persistance/cassandra');
var contacts = require('./src/contacts');

var lodash = require('lodash');

var CassandraPersister = cassandra.CassandraPersister;
var ContactList = contacts.ContactList;

var config = {
    cassandraConnect: {
        contactPoints: ['127.0.0.1'],
        keyspace: 'contacts'
    }
};

var persister = new CassandraPersister(config);
var contactList = new ContactList(persister);

var contacts = [
    {
        id: '35e8a424-bb99-4351-8b87-ff3960010f35',
        name: 'Andrew Elgert',
        details: [
        {
            service: 'email',
            label: 'personal',
            handle: 'andrew.elgert@gmail.com'

        }]
    },
    {
        name: 'Wallace Middleton',
        details: [
        {
            service: 'twitter',
            label: 'personal',
            handle: '@wiredwall'
        }]
    }
];

lodash._(contacts).forEach(function (contact) {
    contactList.addOrUpdate(
        contact, console.log, function(err) {
            console.error("ERROR: " + err)
        });
});

var results = null;

contactList.retrieveAll(function(result) { results = result; }, console.error);

contactList.remove(results.rows[0])

